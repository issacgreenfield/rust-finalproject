# README 
This is the basic process I used to write a web-based GUI program that makes a card-memory game, for Homework #4/Final Project in the Rust programming intensive at PSU 
Author: Issac Greenfield, Winter 2020
Project Name: Analogous Babyings

This program creates a point-and-click, mouse driven, GUI-based memory game. It allows the user to match-up baby animals, and resets when all the pairs have been found.

To run this program, download the ```.zip``` folder, unzip it, cd into the folder through a shell, and enter the command ```cargo run```. The first time it is run will likely take a few minutes to compile. 

The first major issue that I ran into was in implementing the ```Azul``` gui crate. While it seems to work on some machines, MacOS threw some ugly errors when attempting a simple 'hello world' program. It seems to be an open bug, and is unlikely to be fixed in the next couple weeks. Not a good start as I would really like this program to run cross-platform and I am programming it on a Mac. Because of this I decided to switch to ```ggez```. It doesn't technically support Mac, in fact there is a stinging diatribe against the entire apple ecosystem in the README on their github page, but it works well enough for anything that I will be working on here.

For testing I ran unit tests for my external classes, and user-tested the functionality of my main program. I had trouble figuring out good unit testing for my main file, and it would very likely improve the project to do this down the line. 

Running searches through google such as: ```card-matching memory game in "rust"``` and ```memory game using ggez crate``` and other simmilar searches in github, brought up a few other games, but nothing I could find quite like I was doing in ggez. 
The closest other project I could find that uses the ```ggez``` library was from https://github.com/Leinnan/slavic_castles/tree/master/src, which I referenced in my ```game_board``` and ```game_tile``` classes. It appears to be a resource driven card-game, but had better ideas for the style of card resources than I am using. I attempted to refactor my code keeping their design ideas in mind, but have yet to integrate these classes into my program. These classes are intended to add randomization and better OOP design principles.

Stretch goals include the randomization and better OOP design as just mentioned, as well as more diverse images, sound effects, move-counting, and a winning graphic-effect. I am not totally satisfied with how much I was able to accomplish. I was unable to match-up with anyone else, and having another pair of hands would have allowed for much more to be accomplished. I do however like the bones of my project, and will improve it for my portfolio, as I feel it would be a good asset to have once it is polished-up in a few months. 

Images taken from: https://www.freepngs.com/
"PNG images on FreePNGs are released under Creative Commons CC0. PNG images are sourced from the public domain where uploaders have waived their copyright and related or neighbouring rights to these Images. You are free to adapt and use them for your own personal use without attributing the original author or source. Although not required, a link back to FreePNGs is appreciated. "


