// Created by Issac Greenfield for a Rust class
// Winter 2020 Portland State University

// Using the basic outline for functions with help from: https://github.com/ggez/ggez/blob/master/examples/input_test.rs
// Graphic use help: https://github.com/ggez/ggez/blob/efdab624d346532f913bc8b9096dffacb3effbb5/examples/graphics_settings.rs

#![allow(unused)] // TODO: Take this out in production
mod game_board;
mod game_tile;
use ggez::event::{self, MouseButton};
use ggez::graphics::{self, DrawMode, DrawParam, Text};
use ggez::{Context, GameResult};
use std::{env, path, thread, time};

struct MainState {
    pos_x: f32,
    pos_y: f32,
    mouse_down: bool,
    images: Vec<graphics::Image>,
    // Image placement (x_start, y_start)
    image_placement: Vec<(f32, f32)>,
    image_id: Vec<u32>,
    image_width: f32,
    image_height: f32,
    image_show: Vec<bool>,
    image_matched: Vec<bool>,
    // gb: game_board::GameBoard,
    check_matched: u32,
}

impl MainState {
    fn new(ctx: &mut Context) -> GameResult<MainState> {
        let s = MainState {
            pos_x: 100.0,
            pos_y: 100.0,
            mouse_down: false,
            // TODO: use gameboard to gen these
            images: vec![
                graphics::Image::new(ctx, "/5.png")?,
                graphics::Image::new(ctx, "/2.png")?,
                graphics::Image::new(ctx, "/1.png")?,
                graphics::Image::new(ctx, "/6.png")?,
                graphics::Image::new(ctx, "/3.png")?,
                graphics::Image::new(ctx, "/4.png")?,
                graphics::Image::new(ctx, "/4.png")?,
                graphics::Image::new(ctx, "/6.png")?,
                graphics::Image::new(ctx, "/2.png")?,
                graphics::Image::new(ctx, "/5.png")?,
                graphics::Image::new(ctx, "/1.png")?,
                graphics::Image::new(ctx, "/3.png")?,
            ],
            image_placement: vec![
                (10.0, 10.0),
                (205.0, 10.0),
                (400.0, 10.0),
                (595.0, 10.0),
                (10.0, 205.0),
                (205.0, 205.0),
                (400.0, 205.0),
                (595.0, 205.0),
                (10.0, 400.0),
                (205.0, 400.0),
                (400.0, 400.0),
                (595.0, 400.0),
            ],
            image_id: vec![5, 2, 1, 6, 3, 4, 4, 6, 2, 5, 1, 3],
            image_width: 185.0,
            image_height: 185.0,
            image_show: vec![
                false, false, false, false, false, false, false, false, false, false, false, false,
            ],
            image_matched: vec![
                false, false, false, false, false, false, false, false, false, false, false, false,
            ],
            // gb: game_board::GameBoard::new(12),
            check_matched: 0,
        };
        Ok(s)
    }
}

impl event::EventHandler for MainState {
    fn draw(&mut self, ctx: &mut Context) -> GameResult {
        graphics::clear(ctx, [0.1, 0.2, 0.3, 1.0].into());

        let mut squares = Vec::new();
        let mut square_color = graphics::BLACK;
        for i in 0..12 {
            // Check if the image has been clicked for square color
            if self.image_show[i] {
                square_color = graphics::WHITE;
            } else {
                square_color = graphics::BLACK;
            }
            // Add the squares to my grid
            squares.push(graphics::Mesh::new_rectangle(
                ctx,
                DrawMode::fill(),
                graphics::Rect {
                    x: self.image_placement[i].0,
                    y: self.image_placement[i].1,
                    w: 185.0,
                    h: 185.0,
                },
                square_color,
            )?);
            graphics::draw(ctx, &squares[i], (ggez::nalgebra::Point2::new(0.0, 0.0),))?;
            // Handle the images themselves if they have been clicked
            if self.image_show[i] {
                graphics::draw(
                    ctx,
                    &self.images[i],
                    DrawParam::default()
                        .dest(ggez::nalgebra::Point2::new(
                            self.image_placement[i].0,
                            self.image_placement[i].1,
                        ))
                        .scale(ggez::nalgebra::Vector2::new(0.55, 0.55)),
                )?;
            }
        }
        graphics::present(ctx)?;
        // TODO: do this with gameboard
        if self.check_matched > 1 {
            let mut first_image: u32 = 20;
            let mut first_index: usize = 20;
            for i in 0..6 {
                if self.image_show[i] && !self.image_matched[i] {
                    first_image = self.image_id[i];
                    first_index = i;
                    break;
                }
            }
            let mut second_image: u32 = 21;
            let mut second_index: usize = 21;
            for i in 6..12 {
                if self.image_show[i] && !self.image_matched[i] {
                    second_image = self.image_id[i];
                    second_index = i;
                    break;
                }
            }
            if first_image == second_image {
                self.image_matched[first_index] = true;
                self.image_matched[second_index] = true;
            } else {
                for i in 0..12 {
                    if !self.image_matched[i] {
                        self.image_show[i] = false;
                    }
                }
            }
            self.check_matched = 0;
            thread::sleep(time::Duration::from_secs(1));
            for i in 0..12 {
                if !self.image_matched[i] {
                    break;
                }
                if i >= 11 {
                    // Winning state!
                    for i in 0..12 {
                        graphics::clear(ctx, [0.1, 0.2, 0.3, 1.0].into());
                        graphics::present(ctx)?;
                        thread::sleep(time::Duration::from_secs(1));
                        self.image_show[i] = false;
                        self.image_matched[i] = false;
                    }
                }
            }
        }
        Ok(())
    }

    fn mouse_button_up_event(
        &mut self,
        _ctx: &mut Context,
        mouse_click: MouseButton,
        x: f32,
        y: f32,
    ) {
        self.mouse_down = false;
        for i in 0..12 {
            if ((self.image_placement[i].0 < x
                && (self.image_placement[i].0 + self.image_width) > x)
                && (self.image_placement[i].1 < y
                    && (self.image_placement[i].1 + self.image_height) > y))
            {
                self.image_show[i] = true;
                self.check_matched += 1;
            }
        }
    }

    fn update(&mut self, _ctx: &mut Context) -> GameResult<()> {
        // TODO: Use this function as intended
        Ok(())
    }
}

pub fn main() -> GameResult {
    let resource_dir = if let Ok(manifest_dir) = env::var("CARGO_MANIFEST_DIR") {
        let mut path = path::PathBuf::from(manifest_dir);
        path.push("resources");
        path
    } else {
        path::PathBuf::from("./resources")
    };
    let cb = ggez::ContextBuilder::new("input_test", "ggez").add_resource_path(resource_dir);

    let (ctx, event_loop) = &mut cb.build()?;

    let state = &mut MainState::new(ctx)?;
    event::run(ctx, event_loop, state)
}
