#[derive(Copy, Clone)]
pub struct GameTile {
    pub tile_id: u8,
    pub image_num: u8,
    pub is_matched: bool,
}

impl GameTile {
    pub fn new(tile_id_in: u8, image_num_in: u8) -> GameTile {
        GameTile {
            tile_id: tile_id_in,
            image_num: image_num_in,
            is_matched: false,
        }
    }

    /// Checks two tile's ids against another, and returns true if the ids
    /// are the same, false if they are not
    pub fn check_match(self, tile: GameTile) -> bool {
        if self.tile_id == tile.tile_id {
            return true;
        }
        false
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }

    #[test]
    fn test_match() {
        let mut t1 = GameTile::new(1, 1);
        let mut t2 = GameTile::new(1, 1);
        assert_eq!(true, t1.check_match(t2));
        let mut t3 = GameTile::new(1, 1);
        let mut t4 = GameTile::new(2, 2);
        assert_eq!(false, t4.check_match(t3));
    }
}

// could:
// use std::fmt;
// impl fmt::Display for Card {
//     fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
//         let mut result = String::new();
//     ...
// }
