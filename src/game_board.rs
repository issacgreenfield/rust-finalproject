// Copied the design and pattern of flow for this type of object here: https://github.com/Leinnan/slavic_castles/tree/master/src

use crate::game_tile::GameTile;
extern crate rand;
use rand::Rng;
type Row = Vec<GameTile>;

pub struct GameBoard {
    pub tiles: Row,
    // TODO: add tile resources
    pub wrong_guesses: u32,
    pub game_won: bool,
    pub game_over: bool,
}

/// GameBoard keeps track of the state of all tiles. It creates and handles the
/// in there individual rows, as well as if the player has found all matches,
/// or if they have run out of guesses
impl GameBoard {
    pub fn new(num_tiles: u32) -> GameBoard {
        let mut tile_order = Vec::new();
        let mut rand_num = rand::thread_rng();
        println!("here");
        for i in 0..(num_tiles / 2) as usize {
            let mut next_num: u32 = rand_num.gen_range(0, (num_tiles / 2) as u32);
            let mut in_array = true;
            println!("hi");
            while in_array {
                next_num = rand_num.gen_range(0, (num_tiles / 2) as u32);
                in_array = false;
                for n in 0..(tile_order.len() / 2) {
                    println!("hi");
                    if next_num == tile_order[n as usize] {
                        in_array = true;
                        break;
                    }
                }
            }
            tile_order.push(next_num);
            println!("{}", tile_order[i as usize]);
        }
        for i in (num_tiles / 2)..num_tiles {
            let mut next_num: u32 = rand_num.gen_range(0, (num_tiles / 2) as u32);
            let mut in_array = true;
            println!("hi");
            while in_array {
                next_num = rand_num.gen_range(0, (num_tiles / 2) as u32);
                in_array = false;
                for n in (num_tiles / 2)..tile_order.len() as u32 {
                    println!("hi");
                    if next_num == tile_order[n as usize] {
                        in_array = true;
                        break;
                    }
                }
            }
            tile_order.push(next_num);
            println!("{}", tile_order[i as usize]);
        }
        for i in 0..12 {
            println!("{}", tile_order[i as usize]);
        }
        let mut tile_id1 = 1;
        let mut tile1 = GameTile::new(tile_id1, 1);
        let mut tile_id1c = 1;
        let mut tile1c = GameTile::new(tile_id1c, 1);
        let mut tile_id2 = 2;
        let mut tile2 = GameTile::new(tile_id2, 2);
        let mut tile_id2c = 2;
        let mut tile2c = GameTile::new(tile_id2c, 2);
        // let mut tiles_in = vec![GameTile::from(tile1), GameTile::from(tile1c)];
        GameBoard {
            tiles: vec![tile1, tile1c, tile2, tile2c],
            // second_row: Vec::new(),
            // TODO: add tile resources
            wrong_guesses: 0,
            game_won: false,
            game_over: false,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }

    #[test]
    fn test_match() {
        println!("here");
        let mut g1 = GameBoard::new(12);
        assert_eq!(true, g1.tiles[0].check_match(g1.tiles[1]));
        assert_ne!(true, g1.tiles[0].check_match(g1.tiles[2]));
    }
}
